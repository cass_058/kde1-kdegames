# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 1999-01-04 01:41+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: LinuxKorea Co. <kde@linuxkorea.co.kr>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8BIT\n"

#: app.cpp:94 app.cpp:143 board.cpp:222
msgid "Shisen-Sho"
msgstr "마작"

#: board.cpp:223
msgid "Cannot load pixmaps!"
msgstr "픽스맵을 불러올 수 없습니다!"

#: app.cpp:108
msgid "Get &hint"
msgstr "힌트 얻기(&h)"

#: app.cpp:110
msgid "&New game"
msgstr "새 게임(&N)"

#: app.cpp:111
msgid "Res&tart game"
msgstr "게임 다시 시작(&t)"

#: app.cpp:113
msgid "Is game solvable?"
msgstr "게임 진행 가능 여부"

#: app.cpp:115
msgid "&Hall of Fame"
msgstr "명예의 전당(&H)"

#: app.cpp:125
msgid "14x6"
msgstr "14x6"

#: app.cpp:126
msgid "18x8"
msgstr "18x8"

#: app.cpp:127
msgid "24x12"
msgstr "24x12"

#: app.cpp:128
msgid "26x14"
msgstr ""

#: app.cpp:129
msgid "30x16"
msgstr ""

#: app.cpp:133
msgid "Very fast"
msgstr "매우 빠름"

#: app.cpp:134
msgid "Fast"
msgstr "빠름"

#: app.cpp:135 app.cpp:140
msgid "Medium"
msgstr ""

#: app.cpp:136
msgid "Slow"
msgstr "느림"

#: app.cpp:137
msgid "Very slow"
msgstr "매우 느림"

#: app.cpp:139
msgid "Easy"
msgstr "쉬움"

#: app.cpp:141
msgid "Hard"
msgstr "어려움"

#: app.cpp:145
msgid ""
"\n"
"\n"
"by Mario Weilguni"
msgstr ""

#: app.cpp:151
msgid "Si&ze"
msgstr "크기(&z)"

#: app.cpp:152
msgid "S&peed"
msgstr "속도(&p)"

#: app.cpp:153
msgid "&Level"
msgstr "레벨(&L)"

#: app.cpp:154
msgid "Disallow unsolvable games"
msgstr "풀리지 않은 게임은 허용하지 않음"

#: app.cpp:175
msgid "Your time: XX:XX:XX"
msgstr "귀하 시간 : XX:XX:XX"

#: app.cpp:176 app.cpp:273 app.cpp:298
msgid "Cheat mode"
msgstr "계략 모드"

#: app.cpp:264
msgid "This game is solveable"
msgstr "이 게임은 풀 수 있습니다"

#: app.cpp:267
msgid "This game is NOT solveable"
msgstr "이 게임은 풀 수 없습니다"

#: app.cpp:389 app.cpp:421
msgid "End of game"
msgstr "게임 종료"

#: app.cpp:390
msgid "No more moves possible!"
msgstr "더이상 움직일 수 없음!"

#: app.cpp:416
#, c-format
msgid "Congratulations! You made it in %02d:%02d:%02d"
msgstr "축하합니다! 귀하는 %02d:%02d:%02d 만에 문제를 풀었습니다"

#: app.cpp:433
#, c-format
msgid "Your time: %02d:%02d:%02d"
msgstr "진행 시간: %02d:%02d:%02d"

#: app.cpp:444
msgid ""
"You've made in into the \"Hall Of Fame\".Type in\n"
"your name so mankind will always remember\n"
"your cool rating."
msgstr ""
"귀하는 \"명예의 전당\"에 들어섰습니다. 귀하의\n"
"이름을 입력하시면 만인은 귀하의 이름을 항상 기억할 것입니다\n"
"시원한 경기였습니다."

#: app.cpp:447
msgid "Your name:"
msgstr "귀하의 이름:"

#: app.cpp:608 app.cpp:613
msgid "Hall Of Fame"
msgstr "명예의 전당"

#: app.cpp:609
msgid "Shisen-Sho: Hall Of Fame"
msgstr "마작: 명예의 전당"

#: app.cpp:634
msgid "Rank"
msgstr "순위"

#: app.cpp:638
#, fuzzy
msgid "Name"
msgstr "새 게임(&N)"

#: app.cpp:642
msgid "Time"
msgstr "시간"

#: app.cpp:646
#, fuzzy
msgid "Size"
msgstr "크기(&z)"

#: app.cpp:650
msgid "Score"
msgstr "점수"

#~ msgid "28x16"
#~ msgstr "28x16"

#~ msgid "32x20"
#~ msgstr "32x20"
